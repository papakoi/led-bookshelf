#!/usr/bin/env bash

# perform system update
apt update
apt full-upgrade -y
apt autoremove -y
apt install python3-pip python3-venv -y

# set up system user and python environment
useradd --system --groups=gpio,spi led
python_venv=/usr/local/lib/led-bookshelf
mkdir $python_venv
chown -R root:led $python_venv
chmod -R 775 $python_venv
runuser -u led -- python3 -m venv $python_venv
runuser -u led -- $python_venv/bin/python3 -m pip install --no-cache-dir rpi-ws281x

# install led_color tool
installdir=/usr/local/bin
cp led_color.py $installdir/led_color.py
ln -s $installdir/led_color.py $installdir/led_color

# install systemd services
cp *.service /etc/systemd/system
systemctl daemon-reload
systemctl enable led-clear
systemctl enable led-white
