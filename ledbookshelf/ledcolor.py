#!/usr/local/lib/led-bookshelf/bin/python3

import argparse
from rpi_ws281x import PixelStrip, Color


def color_limit(arg):
    try:
        i = int(arg)
    except ValueError:
        raise argparse.ArgumentTypeError('must be an integer')
    if i < 0:
        raise argparse.ArgumentTypeError('cannot be less than 0')
    if i > 255:
        raise argparse.ArgumentTypeError('cannot be greater than 255')
    return i


def step_limit(arg):
    try:
        i = int(arg)
    except ValueError:
        raise argparse.ArgumentTypeError('must be an integer')
    if i < 1:
        raise argparse.ArgumentTypeError('cannot be less than 1')
    return i


def clear_color(led_strip):
    """Clear the LED strip of any colors."""
    set_color(led_strip, red=0, green=0, blue=0)


def set_color(led_strip: PixelStrip, red: int, green: int, blue: int, step: int = 1, reset_len: int = 0):
    """Set the color of every nth pixel of the LED strip.

    :param led_strip: The LED strip to modify.
    :param red: The red color value from 0 to 255.
    :param green: The green color value from 0 to 255.
    :param blue: The blue color value from 0 to 255.
    :param step: Every nth number of pixels to set after the first pixel.
    :param reset_len: The length of each run before resetting the pixel step. This value is ignored when less than 2.
    """
    if reset_len < 2:
        reset_len = led_strip.numPixels()
    for pixel in range(0, led_strip.numPixels()):
        if pixel % reset_len % step == 0:
            led_strip.setPixelColor(pixel, Color(red, green, blue))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clear', action='store_true',
                        help='clear the LED strip of any colors')
    parser.add_argument('-r', '--red', type=color_limit,
                        default=0, help='set the red value')
    parser.add_argument('-g', '--green', type=color_limit,
                        default=0, help='set the green value')
    parser.add_argument('-b', '--blue', type=color_limit,
                        default=0, help='set the blue value')
    parser.add_argument('-s', '--step', type=step_limit,
                        default=1, help='set the color every nth pixel')
    parser.add_argument('-n', '--num-pixels', dest='num_pixels', type=step_limit,
                        default=168, help='the number of pixels in the strip')
    parser.add_argument('-l', '--reset-len', dest='reset_len', type=step_limit,
                        default=28, help='the number of pixels to reset the step')
    args = parser.parse_args()

    if not args.clear and (args.red | args.green | args.blue) == 0 and args.step == 1:
        parser.error('at least one argument is required')

    # 10 uses SPI, /dev/spidev0.0
    # must be in groups gpio and spi to acccess
    rpi_gpio_pin = 10

    led_strip = PixelStrip(args.num_pixels, rpi_gpio_pin)
    led_strip.begin()

    if args.clear:
        clear_color(led_strip)
        led_strip.show()
        quit()

    set_color(led_strip, red=args.red, green=args.green,
              blue=args.blue, step=args.step, reset_len=args.reset_len)
    led_strip.show()


if __name__ == '__main__':
    main()
