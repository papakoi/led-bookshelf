# LED Bookshelf

## Quick Setup

The `install.sh` script has been provided in order to allow for an unattended
install. The target system is a headless Raspberry Pi Zero W running Raspberry
Pi OS Lite. To run, place the contents of this repo onto the target device and
run the following commands as the `pi` user.

```
$ sudo .\install.sh
$ sudo reboot
```

## Notable Characteristics

Each LED run is 28 pixels in length. Current draw for one pixel can go up to 60
mA at full brightness. This means that every 28 pixel run has the potential to
pull 1.68 A. This is within the allowable rating of 24 AWG at 60 °C per [this
table](https://en.wikipedia.org/wiki/American_wire_gauge#Tables_of_AWG_wire_sizes).
That said, the runs cannot be daisy-chained as two runs in series will exceed
the allowable ampacity of 2.1 A.

Each shelf is expected to host at least two runs. A total of 6 runs brings the
total maximum current draw to 12.6 A. The total amount of runs should not exceed
6 as the maximum rated current of the power supply is 15 A.

It should be noted that the LEDs are bright enough that running at 100% load is
usually unnecessary. Current limiting in software is sufficient if adding
additional power taps is undesirable.

## Initial Setup

Download the latest version of Raspberry Pi OS Lite from [this
page](https://www.raspberrypi.org/software/operating-systems/#raspberry-pi-os-32-bit).

Flash the image to an SD card using
[balenaEtcher](https://www.balena.io/etcher/).

Go through the [Setting up a Headless Raspberry
Pi](https://www.raspberrypi.org/documentation/computers/configuration.html#setting-up-a-headless-raspberry-pi)
instructions. Use the following example `wpa_supplicant.conf` file saved to the
boot folder of the SD card. Be sure to also add the `ssh.txt` file to the [boot
folder](https://www.raspberrypi.org/documentation/computers/configuration.html#ssh-or-ssh-txt)
as well in order to enable SSH.

```conf
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
country=US
update_config=1

network={
 ssid="My Home Wifi"
 psk="my cool passphrase"
}
```

---
**NOTE**

The `wpa_supplicant.conf` file should use standard Unix-style `LF` line endings
and not Windows-style `CRLF`.

---

Consider adding the accompanying `config.txt` file to the SD card as well which
has all the options added from the [Adding WS2812B
Support](#adding-ws2812b-support) section.

Remove the SD card from the PC and insert it into and boot the Raspberry Pi.
Wait 5 minutes before proceeding to login via SSH.

There are a number of ways to specify the hostname to use for the login. If the
IP address can be acquired via nmap or router configuration settings, do so.
Otherwise, it might be possible to connect via the default `raspberrypi`
hostname. The following command opens an SSH session using the default username
and host.

```
$ ssh pi@raspberrypi
```

Once in, change the `pi` user password.

```
$ passwd
```

Then, bring the system up-to-date as soon as possible by issuing the following
commands.

```
# apt update
# apt full-upgrade -y
# apt autoremove -y
```

Be sure to get the IPv6 address as well. This should be used to connect instead
of the hostname if the hostname is subject to change or is at risk of a
collision. Check the `wlan` interface:

```
$ ifconfig
```

To change the hostname:

```
# hostnamectl set-hostname led-bookshelf
```

To set the timezone:

```
# timedatectl set-timezone America/Chicago
```

## Adding WS2812B Support

There are a variety of ways to interface with the WS2812B LEDs. This section
covers how to set up SPI as this is generally more straight-forward and leads to
less complications.

While this section covers setup and testing using Python, there is another
option available via .NET IoT libraries. See the [Ws281b
class](https://docs.microsoft.com/en-us/dotnet/api/iot.device.ws28xx.ws2812b?view=iot-dotnet-1.5)
for more information.

In the `/boot/config.txt` file, enable SPI by uncommenting the following line:

```
dtparam=spi=on
```

Set the GPU clock frequency for either the Raspberry Pi Zero and Raspberry Pi 3
to 250 MHz:

```
[pi0]
# Adjust GPU core frequency for SPI clock
core_freq=250

[pi3]
# Adjust GPU core frequency for SPI clock
core_freq=250
```

---
**NOTE**

See the `core_freq` row in [this
table](https://www.raspberrypi.org/documentation/computers/config_txt.html#overclocking)
on overclocking.

---

It doesn't hurt to disable bluetooth in `\boot\config.txt` if it's not being
used. Add the following:

```
dtoverlay=disable-bt
```

After making all the changes to the `\boot\config.txt` file, reboot the Pi:

```
# reboot
```

Install the `pip` Python package manager:


```
# apt install python3-pip
```

Install the `rpi-ws281x` Python package:

```
$ python3 -m pip install --user rpi-ws281x
```

Grab the `strandtest.py` example:

```
$ wget https://raw.githubusercontent.com/rpi-ws281x/rpi-ws281x-python/master/examples/strandtest.py
```

The test script is configured to use PWM. Adjust the script to use SPI instead
as well as any other settings as needed. Once done, run the test script:

```
$ python3 strandtest.py -c
```

## Using systemd

You can use systemd to run services in order to control the LED strip without
requiring human intervention. For a secure system, set up a single user for the
service to run as. The following command creates the `led` user for this purpose
and adds the new user to the `gpio` and `spi` groups in order to gain access to
the SPI.

```
# useradd --system --groups=gpio,spi led
```

The `rpi-ws281x` Python package must also be installed for the user as well.
It's best to set up a Python virtual environment for this purpose.

```
# mkdir /usr/local/lib/led-bookshelf
# chown -R root:led /usr/local/lib/led-bookshelf
# chmod -R 775 /usr/local/lib/led-bookshelf
# runuser -u led -- python3 -m venv /usr/local/lib/led-bookshelf
# runuser -u led -- /usr/local/lib/led-bookshelf/bin/python3 -m pip install --no-cache-dir rpi-ws281x
```

Run the service as the user by adding the user to the `[Service]` section of the
unit file.

```conf
[Service]
Type=oneshot
ExecStop=/usr/local/lib/led-bookshelf/bin/python3 /usr/local/bin/led_color.py --num-pixels=168 --clear
User=led
RemainAfterExit=true
```

---
**NOTE**

You must remove the `User=` option if you wish to run the unit in user mode.

---

Install system unit files in `/etc/systemd/system/`. In order to run the unit
files as other users, remove the `User=` option in the unit file and install the
unit file in `/etc/systemd/user/`. Be sure to add any user that wishes to run
the unit file to the `gpio` and `spi` groups as well as installing the
`rpi-ws281x` Python package.

Once installed, enable the services.

```
# systemctl enable led-white
# systemctl enable led-clear
```
